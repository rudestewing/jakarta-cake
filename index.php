<?php @session_start(); ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      Jakarta Cake Home
    </title>

    <link rel="stylesheet" type="text/css" href="assets/css/master.css" >
    <script src="assets/javascript/main.js"> </script>
</head>

<?php include $_SERVER['DOCUMENT_ROOT']. '/jakarta-cake/config/koneksi.php';  ?>

    <?php include ('include/header.php'); ?>

  <body>



    <div class="main"> <!-- main page untuk load isi konten -->

      <div class="banner">
        <img src="assets/images/banner02.jpg" alt="">
      </div>
      <h4 id="hotlist"> PALING BANYAK DI GEMARI CUSTOMER KAMI </h4>
      <div class="container" style="">
        <div class="container-index">

      <?php $query_show=mysqli_query($con,"SELECT produk.nama_produk,harga_produk,gambar,keterangan,
		                                        detail.id_produk,
                                            count(detail.id_produk) as hot_product
                                            from produk
                                            inner join detail
		                                        on produk.id_produk = detail.id_produk
                                            group by detail.id_produk order by count(detail.id_produk) desc limit 4
                                            ");
      if(mysqli_num_rows($query_show) == 0){

        echo " no database! ";
      } else {
        while($data=mysqli_fetch_array($query_show)){
          $id_produk=$data['id_produk'];
          $nama_produk=$data['nama_produk'];
          $harga_produk=$data['harga_produk'];

          $gambar=$data['gambar'];
          $keterangan=$data['keterangan'];
          ?>

         <div class="items-container">

                 <div class="items">

                   <div class="item-image">
                     <a id="img" href="item-detail.php?id=<?php echo $id_produk; ?>"> <img id="img" src="<?php echo 'http://localhost/tokokue/admin/produk/produk_img/'.$gambar;?>" alt=""> </a>

                      <div class="item-price">

                      <?php // echo $harga_produk; ?>

                      </div>


                   </div>

                   <div class="item-title">
                     <a href="item-detail.php?id=<?php echo $id_produk; ?>"> <p> <?php echo $nama_produk; ?>  </p> </a>
                   </div>

                   <div class="line-item">
                     <hr>
                   </div>
                   <div class="item-description">
                     <p> <?php echo $keterangan; ?></p>
                   </div>
                 </div> <!-- tutup item -->
         </div>
    <?php
            }
          }
    ?>





      </div>
      </div> <!--  TUTUP CONTAINER  -->

    </div> <!--  tutup main page  -->


    <?php include('include/footer.php'); ?> <!--  call footer  -->

  </body>

</html>
