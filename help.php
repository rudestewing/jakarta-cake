<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      Panduan Untuk Pembeli
    </title>
    <link rel="stylesheet"  type="text/css" href="assets/css/master.css">
    <script src="assets/javascript/main.js">

    </script>
  </head>
  <?php  include $_SERVER['DOCUMENT_ROOT'] .'/jakarta-cake/include/header.php'; ?>
  <body>

    <div class="main">
        <div class="container">
          <div class="help">


          <h4 id="help-title"> Cara Berbelanja Di Website Kami. </h4>
          <br>
          <p class="help-description">
            Ikuti Petunjuk Dan Ketentuan Berikut Untuk Dapat Memesan Kue Kami.
            <br>
            <br>
            <br><br><p id="numb">1. Sebelum dapat berbelanja online melalui website kami, anda diharuskan untuk login dahulu. Jadi pastikan anda sudah memiliki akun. </p>
            <br><br><p id="numb">2.	Jika belum memilki akun, anda bisa melakukan pendaftaran terlebih dahulu. </p>
            <br><br><p id="numb">3.	Silahkan klik gambar kue yang anda inginkan kemudian isi jumlah kue yang ingin anda pesan sebelum menambahkan kue yang anda inginkan ke keranjang belanja (Cart). <br> Jumlah pemesanan tidak bisa melebihi 100 </p>
            <br><br><p id="numb">4.	Jika anda ingin mengubah jumlah kue yang anda pesan, anda bisa kembali ke menu produk atau home dan mencari kue yang anda inginkan lalu anda bisa menambahkan jumlah kue tersebut dengan cara memasukan ke keranjang belanja. </p>
            <br><br><p id="numb">5.	Tekan tombol checkout jika anda sudah yakin akan produk kue yang anda pesan. </p>
            <br><br><p id="numb">6.	Metode pembayaran kami melalui Transfer Rekening Bank BCA, untuk mempermudah kami dalam melalukan verifikasi, silahkan anda upload bukti pembayaran anda dengan cara klik tombol upload bukti pembayaran, kemudian submit. </p>
            <br><br><p id="numb">7.	anda bisa melakukan pembatalan transaksi pada menu list transaksi  </p>
            <!-- <br><br><p id="numb">8.	 </p>
            <br><br><p id="numb">9.	 </p> -->
            <br><br><p id="numb">8.	Terima kasih sudah memberikan kepercayaan anda kepada kami.

          </p>

          <a class="back-to-index" href="index.php"> MULAI BERBELANJA </a>
          </div>

        </div>
    </div>

  </body>
  <?php  include $_SERVER['DOCUMENT_ROOT']. '/jakarta-cake/include/footer.php'; ?>

</html>
