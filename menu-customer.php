<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      Profile
    </title>
    <link rel="stylesheet" type="text/css" href="assets/css/master.css">
    <script src="assets/javascript/main.js">
    </script>

  </head>
<?php  include $_SERVER['DOCUMENT_ROOT']. '/jakarta-cake/include/header.php'; ?>
  <body>

    <div class="main">
      <div class="container">
        <div class="container-profile">

          <h4 id="menu">  <?php echo 'Hai, '. $login_user; ?></h4><br>

            <?php if(isset($_SESSION['login_user'])){ ?>

            <a id="menu-button" href="profile.php"> PROFIL ANDA </a><br>

            <!-- <a id="menu-button" href="check-transaction.php"> STATUS TRANSAKSI </a><br> -->
            <a id="menu-button" href="transaction-list.php"> LIST TRANSAKSI  </a>

            <?php  } else {  ?>
              <a id="menu-button" href="index.php"> HOME  </a>

          <?php  } ?>
        </div>

      </div>

  </body>
<?php  include $_SERVER['DOCUMENT_ROOT']. '/jakarta-cake/include/footer.php'; ?>
</html>
