  <?php  include('config/koneksi.php'); ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      Item Detail
    </title>
    <link rel="stylesheet"  style="text/css" href="assets/css/master.css">
    <script src="assets/javascript/main.js">
    </script>
  </head>

  <body>


    <?php  $id_produk=$_GET['id']; ?>

    <?php  include('include/header.php'); ?>



<?php

    if(isset($_POST['add_to_cart'])){

      $id_produk=$_POST['id_produk'];
      $nama_produk=$_POST['nama_produk'];
      $harga_produk=$_POST['harga_produk'];
      $gambar=$_POST['gambar'];
      $keterangan=$_POST['keterangan'];
      $jumlah=$_POST['jumlah'];

      if($jumlah>100){
        $alert = "Maaf pembelian tidak bisa melebihi 100 pcs ";
        echo "<script type='text/javascript'>alert('$alert');</script>";

        // header('location:cart2.php');

      } else {

        $array = [
          'id_produk' => $id_produk,
            'nama_produk' => $nama_produk,
            'harga_produk' => $harga_produk,
            'gambar' => $gambar,
            'keterangan' => $keterangan,
            'jumlah' => $jumlah,
            'subtotal' => ($jumlah * $harga_produk)
        ];

        $_SESSION['cart'][$id_produk] = $array;
        header('location:cart2.php');
      }
    }

?>

    <form class="" action="" method="post">

          <div class="main">
            <div class="container">
              <div class="item-detail">

              <?php
              $query_show=mysqli_query($con,"SELECT * FROM produk WHERE id_produk='$id_produk'");

              if(mysqli_num_rows($query_show) == 0){
                echo "TIDAK ADA DATA!";
              } else {
                $data=mysqli_fetch_array($query_show);

                $id_produk=$data['id_produk'];
                $nama_produk=$data['nama_produk'];
                $harga_produk=$data['harga_produk'];
                $stok=$data['stok'];
                $gambar=$data['gambar'];
                $keterangan=$data['keterangan']; ?>

                <div class="item-detail-image">
                  <a href="#"> <img src="<?php echo 'http://localhost/tokokue/admin/produk/produk_img/' .$gambar ?>" alt=""> </a>
                  <input type="hidden" name="gambar" value="<?php echo $gambar; ?>">
                </div>

                <div class="item-detail-title">
                  <p> <?php  echo $nama_produk; ?></p>
                  <input id="id-produk-hidden" type="hidden" name="id_produk" value="<?php echo $id_produk; ?>">
                  <input id="nama-produk-hidden" type="hidden" name="nama_produk" value="<?php echo $nama_produk; ?>">
                </div>
                <div class="item-detail-description">
                  <p id="description"> <?php echo $keterangan ?></p><br>
                  <input type="hidden" name="keterangan" value="<?php echo $keterangan ?>">
                </div>

                <div class="price">
                  <p> Harga /satuan : Rp. <?php  echo $harga_produk; ?> </p>

                  <input id="harga-produk-hidden" type="hidden" name="harga_produk" value="<?php echo $harga_produk; ?>">
                </div>



                  <div class="qty-input">

                    <?php if(isset($_SESSION['login_user'])) { ?>
                    <?php
                      if(isset($_SESSION['cart'][$id_produk])){
                        $qty = $_SESSION['cart'][$id_produk]['jumlah'];
                      } else {
                        $qty = 1;
                      }


                    ?>


                      <p> Qty : <input type="number" name="jumlah" value="<?php echo $qty; ?>" placeholder="1"> <input id="button" type="submit" name="add_to_cart" value="ADD TO CART"> | <a href="index.php"> <button id="btn-back" href="insert-cart.php" type="button" name="button"> CANCEL</button></a>

                      </p>


                    <?php } else {  ?>
                      <p> anda harus login dulu untuk membeli produk ini
                      </p>

                    <?php   } ?>
                    <?php
                    $query_check=mysqli_query($con,"SELECT stok FROM produk WHERE id_produk='$id_produk'");

                      $data=mysqli_fetch_assoc($query_check);
                      $stok=$data['stok'];

                      if($stok <= 0 ){ ?>
                        <br><p> Kue ini Tidak Ready Stock. Tapi Anda Masih Bisa Melakukan Pemesanan  </p>
                      <?php }  else { ?>
                        <br><p> Ready Stock : <?php echo $stok; ?> unit </p>
                    <?php  }?>

                  </div>

              <?php
              }
              ?>


              </div>
            </div>

          </div>


          <?php  include('include/footer.php'); ?>


    </form>


  </body>
</html>
