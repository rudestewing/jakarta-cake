<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      DAFTAR TRANSAKSI
    </title>

  </head>
  <link rel="stylesheet"  type="text/css" href="assets/css/master.css">
  <script src="assets/javascript/main.js"></script>

  <?php  include $_SERVER['DOCUMENT_ROOT'] .'/jakarta-cake/include/header.php'; ?>

  <body>

        <div class="main">
          <div class="container">

                    <div class="transaction-list">
                      <h4> RIWAYAT PEMESANAN </h4>
                      <?php// echo $login_id; ?>
                      <div class="transaction-list-column-label">
                         <label id="id-invoice"> ID Invoice </label>
                         <label id="tgl-invoice"> Tanggal Invoice </label>
                         <label id="tagihan"> Tagihan </label>
                         <label id="status"> Status </label>
                         <label id="action"> action </label>
                       </div>


                       <?php
                        $query_show_list=mysqli_query($con,"SELECT  invoice.id_invoice,tgl_invoice,total,tagihan,status,
                                                              transaksi.id_customer,
                                                              customer.nama_customer,alamat,
                                                              invoice.id_kota,
                                                              kota.biaya,nama_kota
                                                              FROM invoice
                                                              INNER JOIN transaksi
                                                              ON invoice.id_transaksi = transaksi.id_transaksi
                                                              INNER JOIN customer ON customer.id_customer = transaksi.id_customer
                                                              INNER JOIN kota ON kota.id_kota = customer.id_kota
                                                              WHERE customer.id_customer = '$login_id'
                                                              ORDER BY invoice.id_invoice DESC
                                                              ") ;

                    if(mysqli_num_rows($query_show_list) == 0 ){ ?>

                     <div class="transaction-list-value">
                       <label id="id-invoice"> No Data </label>
                     </div>

                    <?php  } else {
                      while($data=mysqli_fetch_array($query_show_list)){
                        $id_invoice=$data['id_invoice'];
                        $tgl_invoice=$data['tgl_invoice'];
                        $tagihan=$data['tagihan'];
                        $status=$data['status'];


                        ?>

                       <div class="transaction-list-value">
                         <label style="border-bottom:1px solid; padding-bottom:5px;" id="id-invoice"> <?php echo $id_invoice; ?> </label>

                         <label style="border-bottom:1px solid; padding-bottom:5px;" id="tgl-invoice"> <?php  echo $tgl_invoice; ?> </label>
                         <label style="border-bottom:1px solid; padding-bottom:5px;" id="tagihan"> <?php  echo $tagihan; ?> </label>
                         <label style="border-bottom:1px solid; padding-bottom:5px;" id="status"> <?php  echo $status; ?></label>

                         <label style="border-bottom:1px solid; padding-bottom:5px;" id="action">
                           <?php
                           if($status=="batal"){ ?>
                              Cancel
                           <?php } else { ?>


                           <a href="#" onclick="open_new_tab('transaction-detail.php?invoice=<?php echo $id_invoice; ?>')"> Lihat Detail  </a>
                           <?php if($status=='lunas'||$status=='kirim'){ ?>

                           <?php } else { ?>
                           <a href="#" onclick="open_new_tab('checkout-confirm.php?invoice=<?php echo $id_invoice; ?>')"> | Upload Bukti  </a>
                         <?php } ?>
                         <?php  } ?>
                         </label>

                       </div>



              <?php }
                    }
                    ?>



      </div>
    </div>


  </body>
  <?php include $_SERVER['DOCUMENT_ROOT']. '/jakarta-cake/include/footer.php'; ?>

</html>
