<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      Jakarta Cake - Register
    </title>
    <link rel="stylesheet" type="text/css" href="assets/css/master.css">
    <script src="assets/javascript/main.js"></script>

  </head>
  <body>
  <?php include ('include/header.php'); ?>

  <div class="main">
    <div class="container">

<form class="" action="register-process.php" method="post">

      <div class="register-form">
        <h4> REGISTER YOUR ACCOUNT </h4>
        <div class="line-3">
          <hr>
        </div>

        <div class="container-register">
          <div class="input-form">

            <?php
            $qry= mysqli_query($con,"SELECT max(id_customer) as maxid from customer");
            $data=mysqli_fetch_array($qry);
            $maxid = $data['maxid'];

            $nourut= (int) substr($maxid,1,4);
            $nourut++;
            $char = "C";
            $newid= $char.sprintf("%04s",$nourut);

             ?>
            <input id="input-value" type="hidden" name="id_customer" value="<?php echo $newid; ?>"> <br><br>
            <label id="input-label" > Full Name </label> <p id="sign"> : </p>  <input id="input-value" type="text" name="nama_customer" placeholder="Insert Your Name Here !" required="required"><br> <br>
            <label id="input-label" > username </label> <p id="sign"> : </p>  <input id="input-value" type="text" name="username" placeholder="username" required="required"><br> <br>
            <label id="input-label"> e-mail </label> <p id="sign"> : </p> <input id="input-value" type="text" name="email" placeholder="Make Your Own Username" required="required"><br><br>
            <label id="input-label"> Password </label> <p id="sign"> : </p> <input id="input-value" type="password" name="password" placeholder="Password  ?" required="required"><br><br>
            <!-- <label id="input-label"> Re-Type Password </label> <p id="sign"> : </p> <input id="input-value" type="text" name="password" placeholder="Password"><br><br>  -->
            <label id="input-label"> Select Your City </label>  <p id="sign"> : </p> <select id="input-value" name="id_kota" required="required">
              <option value="">  </option>
              <?php $query=mysqli_query($con,"SELECT * FROM kota");
              if (mysqli_num_rows($query) == 0) {
                echo '<option value=""> tidak ada data </option>';
              } else {
                while($data=mysqli_fetch_array($query)){
                    echo '<option value=' .$data['id_kota'].  '>' .$data['nama_kota']. '</option>';
                }
              }
               ?>
            </select><br><br>
            <label id="input-label"> Address </label> <p id="sign"> : </p> <textarea id="input-value" name="alamat" rows="5" cols="30" placeholder="alamat"></textarea> <br><br><br><br><br>

            <label id="input-label"> Phone Number </label> <p id="sign"> : </p> <input id="input-value" type="text" name="no_telepon" placeholder="Phone Number"> <br> <br>
          </div>
        </div>

        <button id="btn-register" type="submit" name="register"> REGISTER  </button>

        <p id="note"> already registered ? click login button below </p>

        <a id="btn-login" onclick="div_show()">  LOGIN NOW  </a>

      </div>

</form>

    </div>
  </div>

  <?php include('include/footer.php'); ?>
  </body>
</html>
