<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      Jakarta Cake - Edit Profile
    </title>
    <link rel="stylesheet" type="text/css" href="assets/css/master.css">
    <script src="assets/javascript/main.js"></script>

  </head>
  <body>
  <?php include ('include/header.php'); ?>
  <?php if(!isset($_SESSION['login_user'])){
    header('location:index.php');
  } ?>

  <div class="main">
    <div class="container">

<form class="" action="update-profile.php" method="post">

      <div class="register-form">
        <h4> EDIT PROFILE </h4>
        <div class="line-3">
          <hr>
        </div>

        <?php
          $query_show=mysqli_query($con,"SELECT * FROM customer WHERE id_customer='$login_id'");

          if($query_show){
          $row=mysqli_fetch_assoc($query_show);

          $nama_customer=$row['nama_customer'];
          $username=$row['username'];
          $password=$row['password'];
          $id_kota=$row['id_kota'];
          $alamat=$row['alamat'];
          $no_telepon=$row['no_telepon'];

        } else {

        } ?>

        <div class="container-register">
          <div class="input-form">

            <label id="input-label" > ID CUSTOMER </label> <p id="sign"> : <?php echo $login_id; ?> </p>  <input id="input-value" type="hidden" name="id_customer" value="<?php echo $login_id; ?>" required="required"><br> <br>
            <label id="input-label" > Full Name </label> <p id="sign"> : </p>  <input id="input-value" type="text" name="nama_customer" value="<?php echo $nama_customer; ?>" required="required"><br> <br>
            <label id="input-label"> username </label> <p id="sign"> : </p> <input id="input-value" type="text" name="username" value="<?php echo $username; ?>" required="required"><br><br>
            <label id="input-label"> Password </label> <p id="sign"> : </p> <input id="input-value" type="text" name="password" value="<?php echo $password; ?>" required="required"><br><br>
            <!-- <label id="input-label"> Re-Type Password </label> <p id="sign"> : </p> <input id="input-value" type="text" name="password" placeholder="Password"><br><br>  -->

              <?php $query_nama_kota=mysqli_query($con,"SELECT nama_kota FROM kota WHERE id_kota='$id_kota'");
              if($query_nama_kota) {
              $row_kota=mysqli_fetch_array($query_nama_kota);
              $nama_kota=$row_kota['nama_kota'];
              } else {
              $nama_kota="no data";
              } ?>

            <label id="input-label"> Select Your City </label>  <p id="sign"> : </p> <select id="input-value" name="id_kota" required="required">
              <option value="<?php echo $id_kota; ?>"> <?php echo $nama_kota; ?>  </option>
              <?php $query=mysqli_query($con,"SELECT * FROM kota");
              if (mysqli_num_rows($query) == 0) {
                echo '<option value=""> tidak ada data </option>';
              } else {
                while($data=mysqli_fetch_array($query)){
                    echo '<option value=' .$data['id_kota'].  '>' .$data['nama_kota']. '</option>';
                }
              }
               ?>
            </select><br><br>
            <label id="input-label"> Address </label> <p id="sign"> : </p> <textarea id="input-value" name="alamat" rows="5" cols="30" > <?php echo $alamat; ?></textarea> <br><br><br><br><br>

            <label id="input-label"> Phone Number </label> <p id="sign"> : </p> <input id="input-value" type="text" name="no_telepon" value="<?php echo $no_telepon; ?>"> <br> <br>
          </div>
        </div>

        <button id="btn-register" type="submit" name="update"> UPDATE </button>

        <a id="btn-login" href="index.php">  BACK TO HOME  </a>

      </div>

</form>

    </div>
  </div>

  <?php include('include/footer.php'); ?>
  </body>
</html>
