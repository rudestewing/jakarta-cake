<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      Profile
    </title>
    <link rel="stylesheet" type="text/css" href="assets/css/master.css">
    <script src="assets/javascript/main.js"></script>


  </head>
<?php  include $_SERVER['DOCUMENT_ROOT']. '/jakarta-cake/include/header.php'; ?>

  <body>

        <?php if(!isset($_SESSION['login_user'])){
            header('location:index.php');
         } ?>

    <div class="main">

<?php if(isset($_SESSION['login_user'])) { ?>

  <?php  $query_show_profile=mysqli_query($con,"SELECT * FROM customer WHERE id_customer='$login_id'") ;
    if($query_show_profile){
      $row=mysqli_fetch_array($query_show_profile);
      $nama_customer=$row['nama_customer'];
      $username=$row['username'];
      $password=$row['password'];
      $id_kota=$row['id_kota'];
      $alamat=$row['alamat'];
      $no_telepon=$row['no_telepon'];
    } else {
      echo " no data ! ";
    } ?>
          <div class="container-profile">
            <h4> PROFIL ANDA </h4><br>
            <div class="profile">
              <label id="profile-label" > Full Name </label> <p id="sign"> : </p> <label id="profile-value"> <?php echo $nama_customer; ?></label> <br> <br>
              <label id="profile-label"> username </label> <p id="sign"> : </p> <label id="profile-value"> <?php  echo $username; ?></label> <br><br>

              <?php $query_nama_kota=mysqli_query($con,"SELECT nama_kota FROM kota WHERE id_kota='$id_kota'");
              if($query_nama_kota) {
              $row_kota=mysqli_fetch_array($query_nama_kota);
              $nama_kota=$row_kota['nama_kota'];
              } else {
              $nama_kota="no data";
              } ?>

              <label id="profile-label"> City </label> <p id="sign"> : </p> <label id="profile-value"> <?php  echo $nama_kota; ?></label> <br><br>
              <label id="profile-label"> Address </label> <p id="sign"> : </p> <label id="profile-value"> <?php echo $alamat; ?></label> <br><br><br><br>
              <label id="profile-label"> Phone </label> <p id="sign"> : </p> <label id="profile-value"> <?php  echo $no_telepon; ?> </label> <br><br>


            </div>
            <a id="profile-edit-profile-button" href="edit-profile.php?id=<?php  echo $login_id; ?>"> EDIT PROFIL </a>
          </div>
  <?php } else { ?>

            <div class="container-profile">
              <h4> ANDA BELUM LOGIN </h4>
            </div>

  <?php } ?>
    </div>


  </body>
<?php  include $_SERVER['DOCUMENT_ROOT']. '/jakarta-cake/include/footer.php'; ?>
</html>
