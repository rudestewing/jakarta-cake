<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      Cart
    </title>
  </head>

  <link rel="stylesheet" type="text/css" href="assets/css/master.css">
  <script src="assets/javascript/main.js"> </script>
  <body>

    <?php include('include/header.php'); ?>

        <?php if(!isset($_SESSION['login_user'])){
            header('location:index.php');
         } ?>


    <form class="" action="checkout.php" method="post">
    <div class="main">
      <div class="container">

        <div class="cart">
          <h4> CART </h4>
          <div class="column-label">
             <label id="item"> Item </label>
             <label id="description"> Deskripsi </label>
             <label id="price"> Harga </label>
             <label id="qty"> Jumlah </label>
             <label id="subtotal"> Subtotal </label>
             <label id="action"> action </label>
           </div>

        <?php
        if(isset($_SESSION['cart'])){
        $_SESSION['total_harga_beli'] = 0;
        $_SESSION['total_item'] = 0;

        //for($x=0; $x < count($_SESSION['cart']);$x++){
        foreach($_SESSION['cart'] as $key => $produk){

          $_SESSION['total_harga_beli'] += $produk['subtotal'];
          $_SESSION['total_item'] += $produk['jumlah'];

           ?>
          <div class="cart-detail">
          <div class="item-cart">

            <div class="item-cart-image">
              <img src="<?php echo 'http://localhost/tokokue/admin/produk/produk_img/' . $produk['gambar']; ?>" alt="">
            </div>
            <div class="item-cart-detail">
              <div class="item-cart-title">
                <p>  <?php echo $produk['nama_produk']; ?> </p>
              </div>
              <div class="item-cart-description">
                <p> <?php  echo $produk['keterangan']; ?></p>
              </div>
            </div>


            <div class="item-cart-price">
              <?php  echo $produk['harga_produk']; ?>
            </div>

            <div class="item-cart-qty">
              <?php  echo $produk['jumlah']; ?>
            </div>
            <div class="item-cart-subtotal">
              <?php echo $produk['subtotal']; ?>
            </div>

            <div class="item-cart-action">
              <a href="delete-cart2.php?id=<?php echo $produk['id_produk']; ?>" > x </a>
            </div>
        </div>

              <?php
                }
              } else { ?>

                <div class="item-cart">
                  <div class="item-cart-detail">
                    <div class="item-cart-title">
                      <p>  NO ITEM </p>
                    </div>
                    <div class="item-cart-description">
                      <p> </p>
                    </div>
                  </div>
                </div>
              <?php
                }
              ?>

            </div> <!-- tutup cart list -->


        </div><!--  TUTUP CONTAINER -->
      </div>  <!-- tutup main  -->


      <div class="total">
          <div class="total-label">
            <div class="total-label-qty">
              <label> Total Item :</label>
            </div>
            <div class="total-label-price">
              <label> Total Pembelian :</label>
            </div>
        </div>

        <div class="total-value">
          <div class="total-qty-value">
            <?php  echo $_SESSION['total_item']; ?>
          </div>
          <div class="total-buy-value">
            Rp.
            <?php  echo $_SESSION['total_harga_beli']; ?>
          </div>
        </div>
      </div>
        <div class="button">
          <button id="btn-checkout" type="submit" name="checkout"> CHECKOUT  </button>  <a href="index.php" id="btn-back">  BELI LAGI </a>
        </div>

      </div>

    </div>

    <?php include('include/footer.php'); ?>
    </form>

    


  </body>
</html>
