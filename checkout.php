  <!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>

    </title>
    <link rel="stylesheet" type="text/css" href="assets/css/master.css">


  </head>

  <body>
      <?php include('include/header.php'); ?>

          <?php if(!isset($_SESSION['login_user'])){
              header('location:index.php');
           } ?>

  <form class="" action="checkout-process.php" method="post">

    <div class="main">
      <div class="container">

        <div class="checkout-form">
          <h4> CHECKOUT </h4>


          <p id="description"> Atas Nama </p> <p id="sign"> : </p> <p id="value"> <?php echo $login_user; ?> </p> <br>
          <p id="description"> Dikirim ke </p> <p id="sign"> : </p> <p id="value"> <?php  echo $login_alamat; ?> </p><br><br><br><br>


          <?php if(isset($_SESSION['cart'])){
            $_SESSION['tagihan'] = ($_SESSION['total_harga_beli'] + $login_biaya);

          } ?>

          <p id="description"> Total Item </p> <p id="sign"> : </p> <p id="value"> <?php echo $_SESSION['total_item']; ?> </p> <br>
          <p id="description"> Total Pembelian </p> <p id="sign"> : </p> <p id="value"> <?php echo $_SESSION['total_harga_beli']; ?></p> <br>
          <p id="description"> Biaya Kirim ke Alamat Anda </p> <p id="sign"> : </p> <p id="value"> <?php echo $login_biaya; ?> </p><br>
          <p id="description"> Total Tagihan </p> <p id="sign"> : </p>  <p id="value"> <?php  echo $_SESSION['tagihan']; ?> </p> <br>

          <button id="button" type="submit" name="bayar"> BAYAR SEKARANG </button>
          <a href="cart.php" id="button"> KEMBALI KE CART </a>

          </div>
        </div>
      </div>

    </div>

    <?php if(!isset($_SESSION['login_user'])){
        header('location:index.php');
     } ?>

    <?php include('include/footer.php'); ?>

  </form>
  </body>
</html>
